import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthService {
  constructor() {
    this.auth = [];
    this.lastUpdated = null;
  }

  async fetchAuthData() {
    // I'm pretending to be a database call to fetch data. I take time!
    await sleep(1000);

    this.lastUpdated = Date.now();
    this.auth = [
      {
        apikey: "aebc03a7-1b70-43c3-a407-4608cf7bcdee",
        companyId: 1
      },
      {
        apikey: "2f2f7222-af33-4077-860f-3fba1f54dcac",
        companyId: 2
      }
    ];
  }

  async findByKey(apikey) {
    if (this.shouldFetchAuthData()) {
      await this.fetchAuthData();
    }
    return this.auth.find(auth => auth.apikey === apikey);
  }

  shouldFetchAuthData() {
    return !this.lastUpdated || this.lastUpdated < Date.now() - 1000 * 60 * 60 * 24;
  }
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
