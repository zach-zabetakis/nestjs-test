import { Dependencies, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
@Dependencies(AuthService)
export class AuthGuard {
  constructor(authService) {
    this.authService = authService;
  }

  async canActivate(context) {
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    const auth = await this.authService.findByKey(headers.apikey);
    if (auth) {
      return true;
    } else {
      throw new UnauthorizedException();
    }
  }
}
