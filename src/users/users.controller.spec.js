import { Test } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let app;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).compile();
  });

  describe('createUser', () => {
    it('should return "User Created!"', () => {
      const usersController = app.get(UsersController);
      expect(usersController.createUser()).toBe('User Created!');
    });
  });

  describe('updateUser', () => {
    it('should return "User Updated!"', () => {
      const usersController = app.get(UsersController);
      expect(usersController.updateUser()).toBe('User Updated');
    });
  })
});
