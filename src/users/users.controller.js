import { Bind, Body, Controller, Dependencies, Param, Post, Put } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller("users")
@Dependencies(UsersService)
export class UsersController {
  constructor(usersService) {
    this.usersService = usersService;
  }

  @Post()
  @Bind(Body())
  createUser(body) {
    return this.usersService.createUser(body);
  }

  @Put(":userId")
  @Bind(Param("userId"), Body())
  updateUser(userId, body) {
    return this.usersService.updateUser(userId, body);
  }

}
