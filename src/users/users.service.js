import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {
  createUser(body) {
    return `User Created! Body: ${JSON.stringify(body)}`;
  }

  updateUser(userId, body) {
    return `User ${userId} Updated! Body: ${JSON.stringify(body)}`;
  }
}
